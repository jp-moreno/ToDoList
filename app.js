const express = require("express");
const bodyParser = require("body-parser");
const app = express();

app.set('view engine', 'ejs');
app.use(express.static("public"));

app.use(bodyParser.json() );       
app.use(bodyParser.urlencoded({     
  extended: true
})); 

let x = 0;

let toDoList = [];

addToDo("wash dishes");
addToDo("walk dog");
addToDo("eat food");

function addToDo(text){
	toDoList.push({do:text, id: x});
	x++;
}

//routes
app.get("/", (req, res) => {
	res.render("main", {toDoList: toDoList});
});

app.post("/", (req, res)  => {
	addToDo(req.body.item);
	res.render("main", {toDoList: toDoList});
});

app.delete("/", (req,res)  => {
	let temp = 0
	toDoList.forEach((value, i) => {
		if(value.id == req.body.itext){
			toDoList.splice(i, 1);
		}
		if(typeof value!="undefined")temp++;
	});
});

app.listen(3000, () => {
	console.log("listening");
});