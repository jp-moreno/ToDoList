const input = document.querySelector("input");
const span = document.querySelectorAll("span");


input.addEventListener("keydown", (e) => {
	if(e.which===13 && input.value != ""){
		fetch("/",
			{
				method: "post",
				headers: {'Content-Type': 'application/json'},
				body:JSON.stringify({item: input.value})
			});
		input.value = "";
		location.reload();
	}
});

Array.from(span).forEach((value, i) => {
	value.addEventListener("click", function(){
		span[i].parentElement.remove();
		fetch("/", 
		{
			method: "delete",
			headers: {'Content-Type': 'application/json'},
			body:JSON.stringify({itext: value.parentElement.id})
		});
	});
});